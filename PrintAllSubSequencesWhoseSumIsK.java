import java.util.ArrayList;
import java.util.List;

public class PrintAllSubSequencesWhoseSumIsK {


    private static void printAllSubSeqSumKRec(int index, int[] arr, int target, List<Integer> ds){

        if(index == arr.length){
            if(target == 0){
                printSubSequence(ds);
            }
            return;
        }

        // pick
        ds.add(arr[index]);
        printAllSubSeqSumKRec(index+1, arr, target - arr[index], ds);

        // non-pick
        ds.remove(ds.size() - 1);
        printAllSubSeqSumKRec(index+1, arr, target, ds);
    }

    private static void printSubSequence(List<Integer> ds) {
        if(ds.isEmpty()){
            System.out.println("{ }");
        } else {
            System.out.print("{ ");
            ds.forEach(num -> System.out.print(num +" "));
            System.out.println("}");
        }
    }

    public static void main(String[] args) {

        int[] arr = {3, 1, 2};
        printAllSubSeqSumKRec(0, arr, 3, new ArrayList<>());
    }
}
