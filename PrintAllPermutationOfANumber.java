import java.util.ArrayList;
import java.util.List;

public class PrintAllPermutationOfANumber {

    public static void main(String[] args) {
        int[] nums = {1, 2, 3};
        List<Integer> ds = new ArrayList<>();
        boolean[] visited = new boolean[nums.length];
        printPermutations(nums, visited, ds);
    }

    private static void printPermutations(int[] nums, boolean[] visited, List<Integer> ds) {

        if(ds.size() == nums.length){
            ds.forEach(num -> System.out.print(num+" "));
            System.out.println();
            return;
        }

        for(int i=0; i< nums.length; i++){
            if(!visited[i]){
                visited[i] = true;
                ds.add(nums[i]);
                printPermutations(nums, visited, ds);
                visited[i] = false;
                ds.remove(ds.size() - 1);
            }
        }
    }
}
