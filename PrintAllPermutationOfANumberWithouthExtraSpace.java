import java.util.Arrays;

public class PrintAllPermutationOfANumberWithouthExtraSpace {

    public static void main(String[] args) {
        int[] nums = {1, 2, 3};
        printPermutations(0, nums);
    }

    private static void printPermutations(int ind, int[] nums) {

        if(ind == nums.length){
            Arrays.stream(nums).forEach(num -> System.out.print(num+" "));
            System.out.println();
            return;
        }

        for(int i=ind; i< nums.length; i++){
            swap(ind, i, nums);
            printPermutations(ind+1, nums);
            swap(ind, i, nums);
        }
    }

    private static void swap(int m, int n, int[] nums){
        int temp = nums[m];
        nums[m] = nums[n];
        nums[n] = temp;
    }
}
