import java.util.ArrayList;
import java.util.List;

public class PrintAllArraySubSequences {


    private static void printAllSubSeqRec(int index, int[] arr, List<Integer> ds){

        if(index == arr.length){
            printSubSequence(ds);
            return;
        }

        // pick
        ds.add(arr[index]);
        printAllSubSeqRec(index+1, arr, ds);

        // non-pick
        ds.remove(ds.size() - 1);
        printAllSubSeqRec(index+1, arr, ds);
    }

    private static void printSubSequence(List<Integer> ds) {
        if(ds.isEmpty()){
            System.out.println("{ }");
        } else {
            System.out.print("{ ");
            ds.forEach(num -> System.out.print(num +" "));
            System.out.println("}");
        }
    }

    public static void main(String[] args) {

        int[] arr = {3, 1, 2};
        printAllSubSeqRec(0, arr, new ArrayList<>());
    }
}
